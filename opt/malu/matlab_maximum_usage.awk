#!/usr/bin/awk -f
#
#
#

# Copyright (C) 2015 Alois Schloegl <alois.schloegl@ist.ac.at>

function filename(tb,Y,M)
{
	F = sprintf("/var/spool/matlabaccounting/%sUsage_%s-%02i.log", tb, Y, M);
}

/^=/ {
	# get number of maximum simultaneous
	if (m < $2 + 0) {
		m = $2+0;
	}
}

/^[^=]/	{
	# list of users
	MU[$1]=1;

	# list of machines
	MA[$2]=1;

	# total usage time(sum over all 10minute intervals of "users per interval")
	T++;
}

END {
	# output
	printf("%s",m>0?m:"-");
	m=length(MU); printf(" %3s", m>0?m:"-");
	m=length(MA); printf(" %3s", m>0?m:"-");
	if (T>0)
		printf(" %7.1f",T/6);
	else
		print(" -");
}


