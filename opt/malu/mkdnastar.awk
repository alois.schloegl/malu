#!/usr/bin/gawk -f
#
# extracts license checkout from lsmon64
#
# Copyright (C) 2023 Alois Schlögl, ISTA
#
#

BEGIN {
    IFS=":";
    D=strftime("%Y-%m-%d %T");
}

function myprint() {
     printf("===== %d %s %s =====\n",NumUser,F,D) >> OUT;
     for (k=1; k<=NumUser; k++)
         print(RES[k]) >> OUT;
}

/Feature name/ {
    if (FeatureNo>0) myprint();	# skip first instance
    FeatureNo++;
    # extract feature name, reset count of users, define output file
    F = $(NF);
    F = substr(F,2,length(F)-2);    # get rid of double quotes " 
    NumUser=0;
    MONTH=strftime("%Y-%m");
    OUT="/var/spool/accounting/dnastar/"F"_"MONTH".log"
}

/Maximum concurrent user/ {
    NumLic[F] = $(NF)
}

/Client Information/ {
    ## count number of users of that feature F
    NumUser++;
}

/User name/ {
    U=$(NF);
}
/Host name/ {
    H=$(NF);
    RES[NumUser]="\t"U"\t"H;
}

ENDFILE {
    myprint()
}
