#!/bin/bash
#
# Copyright (C) 2012,2013,2014,2016 Alois Schloegl,IST Austria

# It measures also the usage of Matlab, its toolbox, and Mathematica
# 1) It produces the list of current_matlab_users.
#    Currently an ASCII-based log file, and an HTML file are produced.
#    The ASCII-based log file will eventually be phased out
# 2) It stores the usage into our accounting logs

DEBUGDIR=debug/
for arg in $@; do
        case $arg in
        -h | --help )
                echo -e 'mkdmastar.sh generates log files of dnastar usage.';
                echo -e 'Usage: bioimaging_accounting {optional arguments}';
                echo -e '\t -h, --help\n\t\t\tthis information' ;
                echo -e '\t -p, -r, -R, --real, --production\n\t\t\tgenerate production report (sent to Ekaterina and Roland)';
                echo -e '\t\t\twithout one of these flags, only a test report is generated.';
                echo -e '\t YYYY-MM     reports for month MM in year YYYY\n';
                exit;
                ;;
        -p | -r | -R | --real | --production )
                FLAG_PRODUCTION=1
                DEBUGDIR=
                ;;
        -* )    ## ignore these
                ;;
        * )     ## set YYYY-MM
                YYYYMM=$arg
                ;;
        esac;
done;


# source /etc/malu.d/dnastar.conf

/opt/DNASTARLicenseServer/tools/lsmon64 | /opt/ist/accounting/mkdnastar.awk
