#!/bin/bash
#
# Copyright 2018 Alois Schloegl, IST Austria
#
# This is work in progress - its not finished yet
#
# check all services ans send email if something does not work as expected. 
# This generates a reminder at midnight when matlab is still running.


MALU_STATUS_FILE="/var/spool/malu.status.check.log"
STATUS=0

for arg in $@; do
        case $arg in
        -h | --help )
                echo -e './check_services.sh checks status of license servers'
                echo -e 'Usage: ./check_services.sh [PERIOD_INDICATOR] [flag]'
                echo -e '\t -h, --help\n\t\t\tthis information'
                echo -e '\t -p, --production\n\t\t: send email notifications'
                echo -e '\t -c, --check\n\t\t: compare current status to status file'
                echo -e '\t -u, --update-checkfile\n\t\t: '
                echo -e "FILES:\n\t$MALU_STATUS_FILE\tcontain the last valid status"
                echo -e "\t/etc/malu.d/*.conf\t            configuration files of license servers"
                exit;
                ;;
	-p | -r | -R | --real | --production )
                FLAG_PRODUCTION=1
                DEBUGDIR=
                ;;
        -c | --check )   ## set YYYY
                ${0} > /tmp/malu.status.check.log 2>/dev/null
                $(cmp -s /tmp/malu.status.check.log $MALU_STATUS_FILE)
		if [ $? -ne 0 ]; then 
			if [[ $FLAG_PRODUCTION ]]; then
				diff -uw "$MALU_STATUS_FILE" /tmp/malu.status.check.log | mail -s "LICENSE SERVER FAILURE - Status does not match desired state!!!" "$SENDTO_ADMIN"
			else
	       			echo "ERROR: LICENSE SERVER FAILURE - Status does not match desired state!!!"; 
				diff -uw "$MALU_STATUS_FILE" /tmp/malu.status.check.log
			fi
		else
			echo "License server status ok"
		fi
		exit
                ;;
        -u | --update-checkfile )   ## set YYYY
                ${0} >"$MALU_STATUS_FILE" 2>/dev/null
                exit
                ;;
        * )     ## ignore these
                flag=1;
                ;;
        esac;
done;

echo "===== USBIP@DongleRasp ====="
if usbip list -r 10.15.21.220 &>/dev/null; 
then echo "usbip@donglerasp OK"; 
else echo "usbip@donglerasp not running";
fi

cd /opt/ist/accounting
for f in $(ls /etc/malu.d/*.conf)
do
	source $f
	echo "===== $PRODUCTNAME ====="
	case $f in 
	/etc/malu.d/gurobi.conf )
		#echo "LD_PRELOAD=/home/flex/gurobi810/linux64/lib/libgurobi81.so /home/flex/gurobi810/linux64/bin/gurobi_cl -t | grep 'Token server'"
		#LD_PRELOAD=/home/flex/gurobi903/linux64/lib/libgurobi90.so /home/flex/gurobi903/linux64/bin/gurobi_cl -t | grep 'Token server'
		source $f 
		$LMSTAT | grep 'Token server'
		;;
	/etc/malu.d/MathematicaUserLicense.conf )
		# do nothing 
		;;
	/etc/malu.d/dnastar.conf )
		$LMSTAT | grep "License Hash"
		;;
	/etc/malu.d/vision4d.conf )
		echo "# $LMSTAT "
		/usr/sbin/usbip port || /etc/init.d/haspdongle restart
		eval ${LMSTAT}
		;;
	/etc/malu.d/codemeter.conf )	
		echo "codemeter.service:"
		systemctl is-active codemeter.service 
                echo "codemeter-webadmin.service:"
		systemctl is-active codemeter-webadmin.service 
		;;
        /etc/malu.d/mnova.conf )
		/etc/init.d/mestrelablicserver  status | awk '/mestrelablicserver is running with pid/ {$NF=""; print}'
		;;
	* ) 
		echo "# $LMSTAT -a"
		$LMSTAT -a |grep "\(running\|: UP\|bitplane.*Yes\|MathLM Version:\)" 2>/dev/null
		# | grep "license server" 
		;;
	esac;	
done



