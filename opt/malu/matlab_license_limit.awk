#!/usr/bin/gawk -f 
#
# Helper function for ./*_license_limit.sh
#
# Copyright (C) 2015 Alois Schloegl <alois.schloegl@ist.ac.at>

function seconds(t) {
        ### convert time into seconds ### 
        split(t,a,":");
        return (a[1]*60+a[2])*60+a[3];
}

function difftime(t1,t2) {
        return (seconds(t2) - seconds(t1));
}
 

/TIMESTAMP/ {
        T=$4;
} 


{	
        #print T,$0; next;

        ### R1: ignore duplicated lines ###
        if (L==$0) next; 

        ### R2: If previous line contains DENIED and current line OUT fullfils
        ### exactly the same request (within 5s), reset NEXT (i.e. do not display DENIED)

        #if (NEXT > 0 && F1==$1 && F2==$2 && $3=="OUT:" && F4==$4 && F5==$5) { 
        if (NEXT>0 && difftime(F1,$1)<5 && F2==$2 && $3=="OUT:" && F4==$4 && F5==$5) { 
                NEXT = 0 ;
        }; 
        
        ### R3: There was really a request DENIED, diplay it
        if (NEXT > 0) {
                print T,L; 
                NEXT=0;	# reset
        } 

        ### Identify candidates of DENIED requests ###
        ### R2 checks whether it is really DENIED, or fullfilled within the next line
        if ($3=="DENIED:" && $5 !~ /^schloegl@/ && $0 ~ /Licensed number of users already reached./) { 
                F1=$1;
                F2=$2;
                F4=$4;
                F5=$5; 
                L=$0; 
                NEXT=1; 
        }; 
}
