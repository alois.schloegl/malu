#!/bin/bash
# Reports the maximum licenses uses at any instance in time.
# Usage:
#    matlab_maximum_usage [period]
#
# Copyright (C) 2013,2014,2015 Alois Schloegl, IST Austria <alois.schloegl@ist.ac.at>

source /etc/malu.d/matlab.conf

P=M;		# default interval
YY=2013;	# default year starting the report

for arg in $@; do
        case $arg in
        -h | --help )
                echo -e 'matlab_maximum_usage generates statistics of matlab usage.'
                echo -e 'Usage: ./matlab_maximum_usage.sh [PERIOD_INDICATOR] [flag]'
                echo -e '\t -h, --help\n\t\t\tthis information'
                echo -e '\t PERIOD_INDICATOR:	M montly (default)'
                echo -e '\t\t\t:	B bi-montly'
                echo -e '\t\t\t:	Q quaterly (3 month period)'
                echo -e '\t\t\t:	T trimester (4 month period)'
                echo -e '\t\t\t:	H half-year (6 month period)'
                echo -e '\t\t\t:	Y yearly (12 month period)'
                echo -e '\t20YY: 	report start at year 20YY (default 2013)'
                echo -e '\tflag: any other argumment will yield an extended report'
                exit;
                ;;

        M | B | Q | T | H | Y )
		P=$arg
		;;

        20* )   ## set YYYY
		YY=$arg
		;;

	* )     ## ignore these
		flag=1;
		;;
        esac;
done;


# enable regular expression for filename selection
shopt -s extglob

## time units, units per year
declare -A U
U=([Y]=1 [H]=2 [T]=3 [Q]=4 [B]=6 [M]=12);


## width of hours field depends on interval
#HWIDTH=$(( ${U[$P]} < 7 ? (${U[$P]} < 6 ? 9 : 8) : 7));
HWIDTH=$(( ${U[$P]} < 7 ? 9 : 8));


OUT0=$(printf "\n%26s" "year");
OUT4=$OUT0;
for (( Y = "$YY"; Y <= $(date +%Y); Y++)); do
for (( i = 1; i <= ${U[$P]}; i++)); do
	tmp=$(if [ "$i" == "1" ];  then printf " '%2i" $(($Y-2000)); else printf "%3s" "$P$i"; fi);
	OUT0=$OUT0""$(printf "%4s" $tmp );
	OUT4=$OUT4""$(printf "%$HWIDTH""s" $tmp );
done
done

OUT1=$(printf "\nTable: Peak usage of Matlab and its toolboxes (as of %s %s)." $(date "+%F %T"))""$OUT0;
OUT2=$(printf "\nTable: Number of users using Matlab and its toolboxes (as of %s %s)." $(date "+%F %T"))""$OUT0;
OUT3=$(printf "\nTable: Number of machines running Matlab and its toolboxes (as of %s %s)." $(date "+%F %T"))""$OUT0;
OUT4=$(printf "\nTable: Total usage in hours of Matlab and its toolboxes (as of %s %s)." $(date "+%F %T"))""$OUT4;

TOOLBOXES=`$LMSTAT -a |awk '/^Users/ { sub(/:/,"",$3); print $3"\n" } '`
for tb in `echo $TOOLBOXES #SPSS MathKernel Mathematica`
do
	OUT1=$OUT1$(printf "\n%26s" $tb);
	OUT2=$OUT2$(printf "\n%26s" $tb);
	OUT3=$OUT3$(printf "\n%26s" $tb);
	OUT4=$OUT4$(printf "\n%26s" $tb);

	for (( Y = "$YY"; Y <= $(date +%Y); Y++)); do
	for (( i = 1; i <= ${U[$P]}; i++)); do

		PERIOD=$(printf '%04i%s%02i' $Y $P $i);

		## replace Q1, Q2, Q3, Q4, H1, H2 with appropriate filter
		PERIOD=${PERIOD/B01/-0[12]}
		PERIOD=${PERIOD/B02/-0[34]}
		PERIOD=${PERIOD/B03/-0[56]}
		PERIOD=${PERIOD/B04/-0[78]}
		PERIOD=${PERIOD/B05/-+(09|10)}
		PERIOD=${PERIOD/B06/-1[12]}

		PERIOD=${PERIOD/T01/-0[1-4]}
		PERIOD=${PERIOD/T02/-0[5-8]}
		PERIOD=${PERIOD/T03/-+(09|1[1-2])}

		PERIOD=${PERIOD/Q01/-0[1-3]}
		PERIOD=${PERIOD/Q02/-0[4-6]}
		PERIOD=${PERIOD/Q03/-0[7-9]}
		PERIOD=${PERIOD/Q04/-1[0-2]}

		PERIOD=${PERIOD/H01/-0[1-6]}
		PERIOD=${PERIOD/H02/-+(0[7-9]|1[0-2])}

		PERIOD=${PERIOD/Y01/-[01][0-9]}
		PERIOD=${PERIOD/M/-}

		## analyze log files
		F="/var/spool/matlabaccounting/"$tb"Usage_"$PERIOD".log";
		RESULT=$(./matlab_maximum_usage.awk $F 2>/dev/null || echo ". . . .");
		RES=(${RESULT});

		## format output
		OUT1=$OUT1$(printf "%4s" ${RES[0]});
		OUT2=$OUT2$(printf "%4s" ${RES[1]});
		OUT3=$OUT3$(printf "%4s" ${RES[2]});
		OUT4=$OUT4$(printf "%$HWIDTH""s" ${RES[3]});

		if [ $i -eq $(date +%m) -a $P == M ];
		then
			# expected usage current month
			# https://stackoverflow.com/questions/12381501/how-to-use-bash-to-get-the-last-day-of-each-month-for-the-current-year-without-u
			NDAYS=$(date -d "$(date +%m)/1 + 1 month - 1 day" +"%d");
			#PRED=$(awk -v HOURS="${RES[3]}" -v D="$NDAYS" 'END {printf("%7.1f", HOURS*D*3600*24/(systime()-mktime(strftime("%Y %m 01 0 0 0"))))}' </dev/null);
			#PRED=$(awk -v HOURS="${RES[3]}" -v D="$NDAYS" 'END {printf("%7.1f", HOURS*D*3600*24/(600+systime()-mktime(strftime("%Y %m 01 0 0 0"))))}' </dev/null);
			PRED=$(awk -v HOURS="${RES[3]}" -v D="$NDAYS" 'END {printf("%7.1f", HOURS*D*6*24/sprintf("%.0f",(systime()-mktime(strftime("%Y %m 01 0 0 0")))/600));}' </dev/null);
		fi
	done
	done
	OUT4=$OUT4$PRED;
done

if [[ $flag ]]; then
	echo -e "$OUT2" |tee matlab_users.txt
	echo -e "$OUT3" |tee matlab_machines.txt
fi
echo -e "$OUT1" | tee matlab_maximum_users.txt 
if [[ $flag ]]; then
	echo -e "$OUT4" |tee matlab_usage_hours.txt
fi
echo

