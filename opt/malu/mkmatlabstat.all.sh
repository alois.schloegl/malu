#!/bin/bash
#
# Copyright (C) 2012,2013,2014,2016 Alois Schloegl,IST Austria

# It measures also the usage of Matlab, its toolbox, and Mathematica
# 1) It produces the list of current_matlab_users.
#    Currently an ASCII-based log file, and an HTML file are produced.
#    The ASCII-based log file will eventually be phased out
# 2) It stores the usage into our accounting logs

DEBUGDIR=debug/
for arg in $@; do
        case $arg in
        -h | --help )
                echo -e 'mkmatlabstat.all.sh generates log files of matlab and mathematica usage.';
                echo -e 'Usage: bioimaging_accounting {optional arguments}';
                echo -e '\t -h, --help\n\t\t\tthis information' ;
                echo -e '\t -p, -r, -R, --real, --production\n\t\t\tgenerate production report (sent to Ekaterina and Roland)';
                echo -e '\t\t\twithout one of these flags, only a test report is generated.';
                echo -e '\t YYYY-MM     reports for month MM in year YYYY\n';
                exit;
                ;;
        -p | -r | -R | --real | --production )
                FLAG_PRODUCTION=1
                DEBUGDIR=
                ;;
        -* )    ## ignore these
                ;;
        * )     ## set YYYY-MM
                YYYYMM=$arg
                ;;
        esac;
done;


source /etc/malu.d/matlab.conf

#MATLAB_OUTPUT=$LOGDIRECTORY/$DEBUGDIR""lmstat.output

MATLAB_OUTPUT=$($LMSTAT -a)
#MATLAB_OUTPUT2=$(echo -e "$MATLAB_OUTPUT" | awk '/^Users/ {printf("%-30s %i/%i in use\n",$3,$11,$6);} /, start/ {printf("   %s\t%s %s %5s %5s\n",$1,$7,$8,$9,$10);}' )
#MATLAB_OUTPUT2=$(echo -e "$MATLAB_OUTPUT" | awk '/^Users/ {printf("%-30s %i/%i in use\n",$3,$11,$6);} /, start/' )
MATLAB_OUTPUT2=$(echo -e "$MATLAB_OUTPUT" | awk '/^Users/ {flag=($11 < 11);printf("%-30s %i/%i in use\n",$3,$11,$6);} /, start/ {if (flag) print;}' )
MATLAB_OUTPUT3=$(echo -e "$MATLAB_OUTPUT" | awk '/^Users/ {printf("<b><meter value=%i min=0 max=%i></meter> (%i/%i) <a href=\"/usage?%s\">%s</a></b><br>\n",$11,$6,$11,$6,substr($3,1,length($3)-1),$3);} /, start/ {printf("%s<br>",$0)};' )

TOOLBOXES=$(echo -e "$MATLAB_OUTPUT" | awk '/^Users/ { sub(/:/,"",$3); print $3"\n" } ')


#empty file log file
D=$(date '+%F %T')

CMK_VALUES="";

for tb in $TOOLBOXES;
do
        NumLic=$(awk '/INCREMENT '$tb'/ {print $6; exit} /R2015/ {exit}' "$LICFILE");
        TotLic=$(echo -e "$MATLAB_OUTPUT" | awk '/^Users of '$tb'/ {print $6;exit}');
	TH=$($(dirname $0)/matlab_threshold.awk -v TB=$tb);

	LOGFILE=$LOGDIRECTORY'/'$DEBUGDIR''$tb'Usage_'$(date +%Y-%m)'.log'

	STR=`$LMSTAT -f $tb | grep ", start "`
	NN=`echo "$STR" | grep ", start " | wc -l`

	### for dygraph output
	GF="/var/www/$tb.csv"
	if [[ ! -e "$GF" ]]; then
		echo "Time,$tb" > "$GF";
	fi 	
	echo "$D,$NN" >>"$GF"

	if [ $(( NN + TH )) -gt $TotLic ] ; then
		echo -e "$D\tWarning:\t$tb\t$NN/$NumLic($TotLic) used - less than $TH licenses left" >>/home/flex/log/low.license.matlab.log;
	fi 

	### used for check_mk (CMK)
	echo "echo \"0 ${PRODUCTNAME}-$tb  $tb=$NN;$NumLic;$TotLic;0;$TotLic   ${PRODUCTNAME}/$tb: $NN licenses in use\n\"" > /opt/ist/check_mk_agent/local/600/check_${PRODUCTNAME}_$tb.sh 
	CMK_VALUES=$CMK_VALUES"$tb=$NN;$NumLic;$TotLic;0;$TotLic|";

	### write to log file for accounting 
	if [[ $FLAG_PRODUCTION ]]; then
		echo -e "=====" $NN $tb $D "=====" >> $LOGFILE
		echo "$STR" | grep ", start " >> $LOGFILE
	fi
done
	echo "echo \"0 licenses-${PRODUCTNAME} ${CMK_VALUES::-1}   ${PRODUCTNAME} in available\n\"" > /opt/ist/check_mk_agent/local/600/check_${PRODUCTNAME}.sh 


## check whether this is a replacement
#$LMSTAT -a | awk 'BEGIN {print "last updated:",strftime("%F %T"),"\n";} /^Users/ {print $3, $11"/"$6,"in use"} /, start/ {print} '
(echo ${ANNOUNCEMENT}; date "+last updated: %F %T"; echo -e "$MATLAB_OUTPUT2") |tee /var/www/html/current_matlab_users.log


#$LMSTAT -a | awk 'BEGIN {printf("<html><body><pre>last updated: %s\n\n",strftime("%F %T"));} /^Users/ {print $3, $11"/"$6,"in use"} /, start/ {print} END {print "</pre><META HTTP-EQUIV=\"refresh\" CONTENT=\"300\"></body></html>"}'

(date "+<!DOCTYPE html><html><body><pre>${ANNOUNCEMENT}<br>last updated: %F %T"; echo -e "$MATLAB_OUTPUT2"; echo '</pre><META HTTP-EQUIV="refresh" CONTENT="3;/cgi-bin/current_matlab_users.cgi"></body></html>')|tee /var/www/html/current_matlab_users.html
#(date "+<!DOCTYPE html><html><head><style>p { font-family: \"Courier New\", Monospace; }</style></head> <body><p>last updated: %F %T<br>"; echo -e "$MATLAB_OUTPUT3"; echo '<META HTTP-EQUIV="refresh" CONTENT="3;/cgi-bin/current_matlab_users3.cgi"></p></body></html>')
# (date "+<!DOCTYPE html><html><body><pre>last updated: %F %T"; echo -e "$MATLAB_OUTPUT2"; echo '</pre></body></html>')
 (date "+<!DOCTYPE html><html><head><style>p { font-family: \"Courier New\", Monospace; font-size: 12px;}</style></head> <body><p>${ANNOUNCEMENT}<br>last updated: %F %T<br><a href=\"/available.matlab.licenses.txt\">available matlab licenses</a><br>"; echo -e "$MATLAB_OUTPUT3"; echo '</p></body></html>') | \
 	tee /var/www/html/current_matlab_users3.html


exit

### MATHEMATICA: do something for ###
MMA_OUTPUT=$(/usr/local/Wolfram/MathLM/monitorlm |awk '/^Licenses in Use:/ {flag=1} { if (flag) print }')
for tb in Mathematica MathKernel
do
	NN=$(/usr/local/Wolfram/MathLM/monitorlm | awk '/^'$tb'/ { print $3; exit}' );

	### for dygraph output
	GF="/var/www/$tb.csv"
	if [[ ! -e "$GF" ]]; then
		echo "Time,$tb" > "$GF";
	fi 	
	echo "$D,$NN" >>"$GF"

    LOGFILE='/var/spool/matlabaccounting/'$DEBUGDIR''$tb'Usage_'$(date +%Y-%m)'.log'
    #/usr/local/Wolfram/MathLM9/monitorlm |
    if [[ $FLAG_PRODUCTION ]]; then
	echo -e "$MMA_OUTPUT" | awk '/^'$tb'/ { n++; if (n==1) { printf("===== %i %s %s =====\n",$3,$1,strftime("%F %T",systime())) } else {printf("%s\t%s\t%s\t%s\t%s\n",$4,$5,$1,$2,$3)} }' |sort -u >> $LOGFILE
    fi
done;


## check if any license is used
#if [[ -z $(echo -e "$MATLAB_OUTPUT" | awk '/^Users/ {s+=$11;} END { print s; }') ]] 
#then 
# 	echo -e "$MATLAB_OUTPUT" | mutt -s "[MATLAB] no license used - reboot login2 possible " -c "$SENDTO_ADMIN"
#fi

