#!/bin/bash
# Reports the number of users and minutes Matlab and its toolboxes are uses.
# reports also the number of current licenses, as well as the time all license
# have been in use.
#
# Usage:
#    matlab_isused.sh [period]
#
#   examples: 2013H2
#    ./matlab_isused.sh 2013H2
#    ./matlab_isused.sh 2013Q3
#    ./matlab_isused.sh 2013B4
#    ./matlab_isused.sh 2013T1
#
# Copyright (C) 2013, 2014 Alois Schloegl, IST Austria <alois.schloegl@ist.ac.at>

# process input arguments
if [ $# = "0" ]
then
	PERIOD='201[3-9]'
	PERIOD='201+(4-1[0-2]|4-0[9]|5)'
	PERIOD='201+(4-1[0-2]|5)'
	PERIOD='201+(5-1[0-2]|5-0[3-9]|6)'
else
	PERIOD="$1"
fi

## replace Q1, Q2, Q3, Q4, H1, H2 with appropriate filter
PERIOD=${PERIOD/B1/-0[12]}
PERIOD=${PERIOD/B2/-0[34]}
PERIOD=${PERIOD/B3/-0[56]}
PERIOD=${PERIOD/B4/-0[78]}
PERIOD=${PERIOD/B5/-+(09|10)}
PERIOD=${PERIOD/B6/-1[12]}

PERIOD=${PERIOD/T1/-0[1-4]}
PERIOD=${PERIOD/T2/-0[5-8]}
PERIOD=${PERIOD/T3/-+(09|1[0-2])}

PERIOD=${PERIOD/Q1/-0[1-3]}
PERIOD=${PERIOD/Q2/-0[4-6]}
PERIOD=${PERIOD/Q3/-0[7-9]}
PERIOD=${PERIOD/Q4/-1[0-2]}

PERIOD=${PERIOD/H1/-0[1-6]}
PERIOD=${PERIOD/H2/-+(0[7-9]|1[0-2])}

PERIOD=${PERIOD/current/201+(4-1[0-2]|4-0[9]|5)}

#PERIOD=${PERIOD/LY/(2014-0[7-9]\|2014-1[0-2]\|2015)}

# enable regular expression for filename selection
shopt -s extglob


### OUTPUT ###
echo
echo "mach	total number of machines"
echo "users	total number of users"
echo "max	maximum simultaneously used licenses"
echo "a  	number of maintained (renewed) licenses"
echo "b  	total number of available licenses (including not renewed)"
echo "hours	user hours"
echo "c  	hours that 'max' number of licenses were used"
echo "score	tentative indicator for the need of additional licenses"
echo "next	renewing number of licenses in next period"
echo "toolbox	name of product"
echo
echo "Accounting period: "$PERIOD "(as of "`date "+%F %T)"`
echo "mach	users	max (a/b)	  hours (c)	score	next	toolbox"
#echo -e "\nmach\tusers\tmaximum\thours\ttoolbox in "$PERIOD "(as of "`date "+%F %T)"`
#echo -e "users\tmaximum\thours\ttoolbox in "$PERIOD "up to "`date "+%F %T"`

source /etc/malu.d/matlab.conf

# get list of toolboxes
TOOLBOXES=$($LMSTAT -a |awk '/^Users/ { sub(/:/,"",$3); print $3"\n" } ')

for tb in `echo $TOOLBOXES #MathKernel Mathematica`;
#for tb in `echo MathKernel Mathematica`;
do
	NumLic=$(awk '/INCREMENT '$tb'/ {print $6; exit}' "$LICFILE");
	TotLic=$($LMSTAT -a | awk '/^Users of '$tb'/ {print $6;exit}');

	#echo $NumLic $TotLic $tb
	#continue

	### number of machines, users, and maximum simultaneous users,
	awk -v NumLic="$NumLic" -v TotLic=$TotLic '/^=/ { valid=(($2+0)>0); if (valid) {if (m<$2) {m=$2}; HIS[$2+0]++; C1+=($2>=NumLic); C2+=($2>NumLic); }}  /^[^=]/ { if (valid) {MU[$1]=1; MA[$2]=1; T++;}} END { printf("%3i\t%3i\t%2i (%i/%i)\t%7.1f (%.1f)\t%4.1f\t%4.0f\t'$tb'\n", length(MA), length(MU), m, NumLic,TotLic, T/6,HIS[m]/6, m*5.0/6-NumLic, m*5.0/6); }' /var/spool/matlabaccounting/${tb}Usage_$PERIOD*.log 2>/dev/null
#	awk -v NumLic="$NumLic" -v TotLic=$TotLic '/^=/ { valid=(($2+0)>0); if (valid) {if (m<$2) {m=$2}; HIS[$2+0]++; C1+=($2>=NumLic); C2+=($2>NumLic); }}  /^[^=]/ { if (valid) {MU[$1]=1; MA[$2]=1; T++;}} END { printf("%3i\t%3i\t%2i (%i/%i)\t%7.1f (%.1f, %.1f, %.1f)\t%4.1f\t'$tb'\n", length(MA), length(MU), m, NumLic,TotLic, T/6,C1/6,HIS[m]/6,HIS[m-1]/6, log(T*C1/36+.1)/log(10)); }' /var/spool/matlabaccounting/${tb}Usage_$PERIOD*.log 2>/dev/null
#	awk -v NumLic="$NumLic" -v TotLic=$TotLic '/^=/ { if (m<$2+0) {m=$2+0}; C1+=($2>=NumLic); C2+=($2>NumLic); }  /^[^=]/ { MU[$1]=1; MA[$2]=1; T++;} END { printf("%3i\t%3i\t%2i (%i/%i)\t%10.1f (%.1f,%.1f)\t%4.1f\t'$tb'\n", length(MA), length(MU), m,NumLic,TotLic, T/6,C1/6,C2/6,log(T*C1/36+.1)/log(10)); }' /var/spool/matlabaccounting/${tb}Usage_$PERIOD*.log 2>/dev/null
#	awk -v NumLic="$NumLic" -v TotLic=$TotLic '/^=/ { if (m<$2+0) {m=$2+0}; C1+=($2>=NumLic); C2+=($2>NumLic); }  /^[^=]/ { MU[$1]=1; MA[$2]=1; T++;} END { printf("%3i\t%3i\t%2i (%i/%i)\t%10.1f (%.1f)\t%4.1f\t'$tb'\n", length(MA), length(MU), m,NumLic,TotLic, T/6,C1/6,log(NumLic*C1/120+.0001)/log(10)); }' /var/spool/matlabaccounting/${tb}Usage_$PERIOD*.log 2>/dev/null
done
echo

