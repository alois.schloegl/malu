#!/bin/bash
# Reports the maximum licenses uses at any instance in time.
# Usage:
#    matlab_machine_user [period]
#
# Copyright (C) 2013,2014 Alois Schloegl, IST Austria <alois.schloegl@ist.ac.at>


### process input arguments
if [[ $# -eq 0 ]]
then
        PERIOD='201[3-9]'
else
        PERIOD=$1
fi

## replace Q1, Q2, Q3, Q4, H1, H2 with appropriate filter
PERIOD=${PERIOD/B1/-0[12]}
PERIOD=${PERIOD/B2/-0[34]}
PERIOD=${PERIOD/B3/-0[56]}
PERIOD=${PERIOD/B4/-0[78]}
PERIOD=${PERIOD/B5/-+(09|10)}
PERIOD=${PERIOD/B6/-1[12]}

PERIOD=${PERIOD/T1/-0[1-4]}
PERIOD=${PERIOD/T2/-0[5-8]}
PERIOD=${PERIOD/T3/-+(09|1[1-2])}

PERIOD=${PERIOD/Q1/-0[1-3]}
PERIOD=${PERIOD/Q2/-0[4-6]}
PERIOD=${PERIOD/Q3/-0[7-9]}
PERIOD=${PERIOD/Q4/-1[0-2]}

PERIOD=${PERIOD/H1/-0[1-6]}
PERIOD=${PERIOD/H2/-+(0[7-9]|1[0-2])}

# enable regular expression for filename selection
shopt -s extglob


### read configuration ###
source /etc/malu.d/matlab.conf

#for tb in `/usr/local/MATLAB/R2013a/etc/lmstat -a |awk ' /^Users/ { sub(/:/,"",$3); print $3"\n" } '`
TOOLBOXES=`$LMSTAT -a |awk '/^Users/ { sub(/:/,"",$3); print $3"\n" } '`
#echo $TOOLBOXES
for tb in `echo $TOOLBOXES MathKernel Mathematica`
#for tb in MathKernel Mathematica
do
#	echo $tb
#	echo "$LOGDIRECTORY"/${tb}Usage_$PERIOD.log
	#Q=$(echo "$Q")"\n"$(awk '/^[^=]/ { print $2,$1,"'${tb}'"; }' "$LOGDIRECTORY"/${tb}Usage_$PERIOD*.log 2>/dev/null)
	Q=$Q"\n"$(awk '/^[^=]/ { ver=( ( $3 ~ /\(v/ ) ? $3 : ( ($4 ~ /\(v/ ) ? $4 : $5) ); print $2,$1,ver,"'${tb}'"; }' "$LOGDIRECTORY"/${tb}Usage_$PERIOD*.log 2>/dev/null)
done
echo -e "$Q" | sort | uniq -c

