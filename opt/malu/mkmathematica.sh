#!/bin/bash
#
# Copyright (C) 2012,2013,2014,2016 Alois Schloegl,IST Austria

# It measures also the usage of Matlab, its toolbox, and Mathematica
# 1) It produces the list of current_matlab_users.
#    Currently an ASCII-based log file, and an HTML file are produced.
#    The ASCII-based log file will eventually be phased out
# 2) It stores the usage into our accounting logs

DEBUGDIR=debug/
for arg in $@; do
        case $arg in
        -h | --help )
                echo -e 'mkmatlabstat.all.sh generates log files of matlab and mathematica usage.';
                echo -e 'Usage: bioimaging_accounting {optional arguments}';
                echo -e '\t -h, --help\n\t\t\tthis information' ;
                echo -e '\t -p, -r, -R, --real, --production\n\t\t\tgenerate production report (sent to Ekaterina and Roland)';
                echo -e '\t\t\twithout one of these flags, only a test report is generated.';
                echo -e '\t YYYY-MM     reports for month MM in year YYYY\n';
                exit;
                ;;
        -p | -r | -R | --real | --production )
                FLAG_PRODUCTION=1
                DEBUGDIR=
                ;;
        -* )    ## ignore these
                ;;
        * )     ## set YYYY-MM
                YYYYMM=$arg
                ;;
        esac;
done;


source /etc/malu.d/mathematica.conf

#empty file log file
D=$(date '+%F %T')

### MATHEMATICA: do something for ###
MMA_OUTPUT=$($LMSTAT |awk '/^Licenses in Use:/ {flag=1} { if (flag) print }')
for tb in Mathematica MathKernel #Sub #\ MathKernel"
do
	NN=$(echo -e "$MMA_OUTPUT" | awk '/^'${tb}'/ {N++;}; END {print N+0}' );

	### for dygraph output
	GF="/var/www/${tb}.csv"
	if [[ ! -e "$GF" ]]; then
		echo "Time,$tb" > "$GF";
	fi 	
	echo "$D,$NN" >>"$GF"

    LOGFILE="/var/spool/matlabaccounting/${DEBUGDIR}${tb}Usage_$(date +%Y-%m).log"
    #/usr/local/Wolfram/MathLM9/monitorlm |
    if [[ $FLAG_PRODUCTION ]]; then
	#echo -e "$MMA_OUTPUT" | awk -v NN=$NN '$5 ~ /^[1-9]/ {$5="-";} /^'${tb}'/ { n++; if (n==1) printf("===== %i %s %s =====\n",NN,$1,strftime("%F %T",systime())); printf("%s\t%s\t%s\t%s\t%s\n",$4,$5,$1,$2,$3) }' |sort -u >> $LOGFILE
    	echo -e "$MMA_OUTPUT" | awk -v NN=$NN -v TB=$tb 'BEGIN {printf("===== %i %s %s =====\n",NN,TB,strftime("%F %T",systime()))}; $5 ~ /^[1-9]/ {$5="-";} /^'${tb}'/ { printf("%s\t%s\t%s\t%s\t%s\n",$4,$5,$1,$2,$3) }' | sort -u >> $LOGFILE
    else
    	echo -e "$MMA_OUTPUT" | awk -v NN=$NN -v TB=$tb 'BEGIN {printf("===== %i %s %s =====\n",NN,TB,strftime("%F %T",systime()))}; $5 ~ /^[1-9]/ {$5="-";} /^'${tb}'/ { printf("%s\t%s\t%s\t%s\t%s\n",$4,$5,$1,$2,$3) }'
    fi
done;


## check if any license is used
#if [[ -z $(echo -e "$MATLAB_OUTPUT" | awk '/^Users/ {s+=$11;} END { print s; }') ]] 
#then 
# 	echo -e "$MATLAB_OUTPUT" | mutt -s "[MATLAB] no license used - reboot login2 possible " -c "$SENDTO_ADMIN"
#fi

