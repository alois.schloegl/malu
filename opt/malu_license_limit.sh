#!/bin/bash
#
# Checks for denied license requests, and sends email if there where any.
# Remark: there was at least on case, when a "license denied" was immediately followed
#    by a license granted. That means, there are false positive reports.
#    One needs to check on a case by case bases by.
#          grep -A1 DENIED /home/flex/log/flexlm.matlab*.log
#
# Usage:
#    ./matlab_license_limit
#
# Files:
#   matlab.denied.md5   contains checksum of previous check, it is used to identify changes.
#   /home/flex/log/matlab*.log  matlab license log files
#
# Copyright (C) 2014,2015,2016 Alois Schloegl, IST Austria <alois.schloegl@ist.ac.at>


source /etc/malu.d/matlab.conf

for arg in $@; do
        case $arg in
        -h | --help )
                echo -e 'mkmatlabstat.all.sh generates log files of matlab and mathematica usage.';
                echo -e 'Usage: bioimaging_accounting {optional arguments}';
                echo -e '\t -h, --help\n\t\t\tthis information' ;
                echo -e '\t -p, -r, -R, --real, --production\n\t\t\tgenerate production report (sent to Ekaterina and Roland)';
                echo -e '\t\t\twithout one of these flags, only a test report is generated.';
                echo -e '\t YYYY-MM     reports for month MM in year YYYY\n';
                exit;
                ;;
        -p | -r | -R | --real | --production )
                FLAG_PRODUCTION=1
                DEBUGDIR=
                ;;
        -* )    ## ignore these
                ;;
        * )     ## set YYYY-MM
                YYYYMM=$arg
                ;;
        esac;
done;


echo "===== Start of monitoring: 19:43:36 (lmgrd) TIMESTAMP 5/18/2014 =====";

for CONF in `ls /etc/malu.d/*.conf`;
do
        source $CONF

        SUITE=$(basename "$CONF" .conf)
        echo ===$CONF:$SUITE===

	F="${SUITE}.denied.sha1"
	F2="${SUITE}.low.sha1"
	
	CHKSUM=`cat $F`;
	LL="/var/spool/malu/low.license.${SUITE}.log"
	CHKSUM2=`cat $F2`;

	###
	# Report first time stamp - in order to document the time period analysed -
	#   then, the list of denied license requests.
	# Then compute checksum and store it in matlab.denied.sha1
	###

	# LOG=$(awk '/TIMESTAMP/ {T=$4;} { if (($3=="DENIED:")) { print T,$0 }) ' /home/flex/log/flexlm.matlab*.log)
	#   is not good enough, because there are often cases, that a DENIED entry is followed by an OUT entry
	#   at the same second, by the same user, for the same license. It's save to assume the use got the license
	#   and this would cause a false positive report of a DENIED message

	###LOG=$(awk '/TIMESTAMP/ {T=$4;} { if (L==$0) next; if (NEXT>0 && F1==$1 && F2==$2 && $3=="OUT:" && F4==$4 && F5==$5) {NEXT=0;}; if (NEXT>0) {print T,L; NEXT=0;} if ($3=="DENIED:" && $5 !~ /^schloegl@/ && $0 ~ /Licensed number of users already reached./) { F1=$1;F2=$2;F4=$4;F5=$5; L=$0; NEXT=1; }; }' /home/flex/log/flexlm.matlab*.log)
	LOG=$(awk -f ./matlab_license_limit.awk /home/flex/log/flexlm.matlab*.log)

	echo -e "$LOG"
	echo -e "###\n### FOR FULL REPORT ON LOW LICENSES, RUN ###\ncat $LL\n###"
	tail "$LL"

	if [[ $FLAG_PRODUCTION && ( $(echo -e "$LOG" | sha1sum) != $CHKSUM ) ]];
	then
		echo checksum has changed
		echo -e "$LOG" | sha1sum > "$F"
		echo -e "$LOG" | tail | mutt -s '[MALU:'${SUITE}'] LICENSE DENIED - LIMIT EXCEEDED !!!' $SENDTO_LICENSE_DENIED $SENDTO_ADMIN
	fi

	if [[ $FLAG_PRODUCTION && ( $(cat "$LL" | sha1sum ) != $CHKSUM2 ) ]];
	then
		echo checksum has changed
		cat "$LL" | sha1sum > "$F2"
		tail $LL | mutt -s '[MALU:'${SUITE}'] LICENSE LIMIT LOW !!!' $SENDTO_LICENSE_DENIED $SENDTO_ADMIN
		#echo '[MATLAB] LICENSE LIMIT LOW !!!'  ## for debugging
	fi

done
