#!/bin/sh 
#
# Install script for MALU
#
# Copyright (C) 2014,2015,2016 Alois Schloegl
# Copyright (C) 2014,2015,2016 IST Austria

# generate directories
mkdir -p /etc/malu.d		# configuration directory
mkdir -p /var/spool/malu/	# usage logs
mkdir -p /var/log/malu/		# log of license manager
mkdir -p /opt/malu/		# 

# select template configuration file for flexlm, mathlm and sentinel license server
# and adapt options
cp etc/malu.d/template.flexlm.conf.orig /etc/malu.d/matlab.conf

### setup crontab entries
echo "" >> /etc/crontab
echo "### malu: enable license log in 10 minute intervals " >> /etc/crontab
echo "#*/10 *  * * *   root    (/opt/malu/malu.log.all.sh -p &>/dev/null)" >> /etc/crontab

echo "" >> /etc/crontab
echo "### malu: daily check of license limit" >> /etc/crontab
echo "#32 2    * * *   root    cd /opt/malu && ./matlab_license_limit.sh --production &> /dev/null" >> /etc/crontab


